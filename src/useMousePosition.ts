import { useState, useEffect } from "react";

export interface Position {
    x: number;
    y: number;
}

export const useMouseTracker = () => {
    const [position, setPosition] = useState<Position>({ x: 0, y: 0 })


    useEffect(() => {
        const mouseEventListener = (event: MouseEvent) => {
            setPosition(event)
        }

        window.addEventListener("mousemove", mouseEventListener)

        return () => {
            window.removeEventListener("mousemove", mouseEventListener)
        }
    }, [])
    return position
}

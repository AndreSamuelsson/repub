import React from "react";
import { unstable_createResource } from "react-cache";
import _ from "lodash";

const textResource = unstable_createResource((path: string) =>
  fetch(path).then(r => r.text())
);

const audioResource = unstable_createResource<string, HTMLAudioElement>(
  path => {
    return new Promise((resolve, reject) => {
      const audio = document.createElement("audio");
      audio.src = path;
      audio.onloadeddata = () => resolve(audio);
      audio.onerror = reject;
      document.body.append(audio);
    });
  }
);

interface Props {
  file: string;
}

const tags: Record<string, { type: string; resource: string }> = {
  "183:8": {
    type: "audio",
    resource: "/plopp.mp3"
  }
};

export const TextView = ({ file }: Props) => {
  const text = textResource.read(file);
  const audio1 = _.range(1, 100).map(i =>
    audioResource.read(`/plopp.mp3?${i}`)
  );
  const audio2 = _.range(1, 100).map(i => audioResource.read(`/fart.mp3?${i}`));
  //const audio2 = audioResource.read("/nyfraga.mp3");
  //const audio3 = audioResource.read("/fart.mp3");

  const activateWord = (rowIndex: number, wordIndex: number) => {
    //if (tags[`${rowIndex}:${wordIndex}`]) {
    //console.log(rowIndex, wordIndex);
    const x = _.sample(audio1.concat(audio2));
    if (x) {
      x.play();
    }
    //}
  };

  return (
    <div>
      {text.split("\n").map((line, rowIndex) => (
        <div key={rowIndex}>
          {line.split(/\b/g).map((word, wordIndex) => (
            <Word
              word={word}
              key={wordIndex}
              rowIndex={rowIndex}
              wordIndex={wordIndex}
              activateWord={activateWord}
            />
          ))}
        </div>
      ))}
    </div>
  );
};

interface WordProps {
  word: string;
  wordIndex: number;
  rowIndex: number;
  activateWord: (rowIndex: number, wordIndex: number) => void;
}

const Word = ({ word, wordIndex, rowIndex, activateWord }: WordProps) => {
  return (
    <span
      onMouseOver={() => activateWord(rowIndex, wordIndex)}
      onClick={() => console.log(rowIndex, wordIndex)}
      style={{ backgroundColor: tags[`${rowIndex}:${wordIndex}`] ? "red" : "" }}
    >
      {word}
    </span>
  );
};

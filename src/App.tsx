import React, { Suspense, useState, useEffect } from "react";
import "./App.css";
import { useMouseTracker } from "./useMousePosition";
import { TextView } from "./TextView";

const App = () => {
  const { x, y } = useMouseTracker();

  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <TextView file="/smalice.txt" />
      </Suspense>
    </div>
  );
};

export default App;

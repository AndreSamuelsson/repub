import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

const container = document.getElementById("root");
const root = (ReactDOM as any).createRoot(container);
root.render(<App />);
